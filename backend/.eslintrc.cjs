module.exports = {
    'env': {
        'browser': false,
        'es2021': true,
        'node': true,
    },
    'plugins': ['jest'],
    'extends': ['plugin:jest/all'],
    'parserOptions': {
        'ecmaVersion': 13,
        'sourceType': 'module'
    },
    'ignorePatterns': ['/test/*'],
    'rules': {
        'indent': [
            'error', 4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'never'
        ],
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error',
        'jest/require-hook': 0
    },
}
