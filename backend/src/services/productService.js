import productDao from '../dao/productDao.js'

const validateProduct = (product) => {
    return product['name'] && product['price']
}

const insertProduct = async (product) => {
    if (!validateProduct(product)) {
        const err = new Error('Invalid product')
        err.name = 'UserError'
        throw err
    } else {
        return await productDao.insertProduct(product)
    }
}

const updateProduct = async (product) => {
    if (!validateProduct(product) || !product.id) {
        const err = new Error('Invalid product')
        err.name = 'UserError'
        throw err
    } else {
        return await productDao.updateProduct(product)
    }
}

const deleteById = async (id) => {
    const result = await productDao.deleteById(id)
    if (result.rowCount === 1) {
        console.log(`Product with id: ${id} deleted.`)
        return result
    } else {
        const err = new Error('Internal error')
        err.name = 'dbError'
        throw err
    }
}

const findAll = async () => {
    const products = await productDao.findAll()
    return products.rows
}

const findOne = async (id) => {
    const product = await productDao.findOne(id)
    return product
}

export default { 
    findAll,
    findOne, 
    insertProduct,
    updateProduct,
    deleteById
}