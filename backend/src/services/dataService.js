import dataDao from '../dao/dataDao.js'

export const insertData = async (data) => {
    if (!data.primary || !data.secondary) {
        const err = new Error('Invalid data')
        err.name = 'UserError'
        throw err
    } else {
        const response = await dataDao.insertData(data)
        return response.rows
    }
}

// Creates chart.js compatible data-arrays from individual db-rows. 
export const getData = async () => {
    const data = await dataDao.getData()
    const primary = [0,0,0,0,0,0,0]
    const secondary = [0,0,0,0,0,0,0]
    data.rows.forEach((row) => {
        primary[row.primary_number] += 1
        secondary[row.secondary_number] += 1
    })
    return { primary, secondary}
}