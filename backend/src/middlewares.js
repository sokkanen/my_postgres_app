import jwt from 'jsonwebtoken'

export const checkContentType = (req, res, next) => {
    if (req.method === 'POST' && req.headers['content-type'] !== 'application/json') {
        res.status(415).send('Unsupported Media Type')
    }
    next()
}

export const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: 'No one here' })
}

export const errorHandler = (error, _req, res, next) => {
    if (error.name === 'JsonWebTokenError') {
        res.status(401).send({ error: 'unauthorized' })
    } else if (error.name === 'dbError') {
        res.status(500).send({ error: error.message })
    } else if (error.name === 'UserError') {
        res.status(400).send({ error: error.message })
    } else if (error.name === 'AuthError') {
        res.status(401).send({ error: error.message })
    }
    console.error(error)
    next(error)
}

export const allowCors = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'localhost')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
}

export const authorize = (req, res, next) => {
    const token = req.cookies['app-token']
    const decodedToken = jwt.verify(token, process.env.APP_SECRET)
    if (!token || !decodedToken.username) {
        res.status(401).send('Token missing or invalid')
    } else {
        next()
    }
}