import express from 'express'
import dotenv from 'dotenv'
import 'express-async-errors'
import cookieParser from 'cookie-parser'

import productRouter from './api/productRouter.js'
import userRouter from './api/userRouter.js'
import protectedRouter from './api/protectedRouter.js'
import rootRouter from './api/rootRouter.js'
import dataRouter from './api/dataRouter.js'
import { 
    unknownEndpoint, 
    errorHandler,
    allowCors
} from './middlewares.js'
import { createTables } from './db/db.js'

const isDev = process.env.NODE_ENV === 'dev'

const app = express()
isDev && dotenv.config() && app.use(allowCors)
app.use(express.json())
app.use(express.static('build'))
app.use(cookieParser())
app.use('/', rootRouter)
app.use('/products', productRouter)
app.use('/user', userRouter)
app.use('/protected', protectedRouter)
app.use('/data', dataRouter)
app.use(unknownEndpoint)
app.use(errorHandler)

const APP_PORT = process.env.APP_PORT || 8080

process.env.NODE_ENV !== 'test' && 
createTables() && 
app.listen(APP_PORT,() => {
    console.log(`Listening to ${APP_PORT}.`)
})

export default app