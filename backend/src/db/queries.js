const createProductTable = `
    CREATE TABLE IF NOT EXISTS "products" (
	    "id" VARCHAR(36) NOT NULL,
	    "name" VARCHAR(100) NOT NULL,
	    "price" INTEGER NOT NULL,
	    PRIMARY KEY ("id")
    );`

const createCustomerTable = `
    CREATE TABLE IF NOT EXISTS "customer" (
	    "id" VARCHAR(36) NOT NULL UNIQUE,
	    "username" VARCHAR(100) NOT NULL UNIQUE,
	    "passhash" VARCHAR(100) NOT NULL,
	    PRIMARY KEY ("id")
    );`

const createDataTable = `
	CREATE TABLE IF NOT EXISTS "querydata" (
		"id" VARCHAR(36) NOT NULL UNIQUE,
		"primary_number" INTEGER NOT NULL,
		"secondary_number" INTEGER NOT NULL,
		PRIMARY KEY ("id")
	);`

export default { 
    createProductTable,
    createCustomerTable,
    createDataTable
}