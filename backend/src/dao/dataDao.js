import { v4 as uuidv4 } from 'uuid'
import executeQuery from '../db/db.js'

// Should be modified to store also user ID to validate if someone has answered or not...
const insertData = async (data) => {
    console.log(data)
    const params = [uuidv4(), ...Object.values(data)]
    console.log('Inserting new data')
    const result = await executeQuery(
        'INSERT INTO "querydata" ("id","primary_number","secondary_number") VALUES ($1, $2, $3) RETURNING "primary_number","secondary_number";', 
        params
    )
    console.log('New data inserted successfully.')
    return result
}

const getData = async () => {
    console.log('Requesting for all data...')
    const result = await executeQuery(
        'SELECT * FROM "querydata";'
    )
    console.log(`Found ${result.rows.length} data entries.`)
    return result
}

export default { insertData, getData }