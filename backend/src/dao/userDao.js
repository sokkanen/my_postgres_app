import { v4 as uuidv4 } from 'uuid'
import executeQuery from '../db/db.js'

const insertUser = async (user) => {
    const params = [uuidv4(), ...Object.values(user)]
    console.log(`Inserting a new user ${user.username}...`)
    const result = await executeQuery(
        'INSERT INTO "customer" ("id","username","passhash") VALUES ($1, $2, $3);', 
        params
    )
    console.log('New user inserted successfully.')
    return result
}

const findUser = async (username) => {
    console.log(`Searching for user ${username}`)
    const result = await executeQuery('SELECT * FROM "customer" WHERE username = $1;', [username])
    return result.rows[0]
}

export default {
    insertUser,
    findUser
}