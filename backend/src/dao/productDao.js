import { v4 as uuidv4 } from 'uuid'
import executeQuery from '../db/db.js'

const insertProduct = async (product) => {
    const params = [uuidv4(), ...Object.values(product)]
    console.log(`Inserting a new product ${params[0]}...`)
    const result = await executeQuery(
        'INSERT INTO "products" ("id","name","price") VALUES ($1, $2, $3) RETURNING id;', 
        params
    )
    console.log(`New product ${params[0]} inserted successfully.`)
    return result
}

const updateProduct = async (product) => {
    const params = [product.name, product.price, product.id]
    console.log(`Updating a product ${params[0]}...`)
    const result = await executeQuery(
        'UPDATE "products" SET name=$1, price=$2 WHERE id=$3;', 
        params
    )
    console.log(`Product ${params[0]} updated successfully.`)
    return result
}

const findAll = async () => {
    console.log('Requesting for all products...')
    const result = await executeQuery(
        'SELECT * FROM "products";'
    )
    console.log(`Found ${result.rows.length} products.`)
    return result
}

const findOne = async (id) => {
    console.log(`Requesting a product with id: ${id}...`)
    const result = await executeQuery(
        'SELECT * FROM "products" WHERE id = $1;', [id]
    )
    console.log(`Found ${result.rows.length} products.`)
    if (result.rowCount !== 1) {
        const err = new Error('Internal error')
        err.name = 'dbError'
        throw err
    }
    return result.rows[0]
}

const deleteById = async (id) => {
    console.log(`Deleting product with id: ${id}`)
    const params = [id]
    const result = await executeQuery(
        'DELETE FROM "products" WHERE id = $1;', params
    )
    return result
}

export default {
    insertProduct,
    findAll,
    findOne,
    updateProduct,
    deleteById
}