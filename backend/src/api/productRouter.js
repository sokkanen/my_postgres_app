import { Router } from 'express'
import productService from '../services/productService.js'
import { checkContentType } from '../middlewares.js'

const router = Router()
router.use(checkContentType)

router.post('/', async (req, res) => {
    const product = req.body
    const storedProduct = await productService.insertProduct(product)
    if (storedProduct.rowCount === 1) {
        res.status(201).send(storedProduct.rows[0])
    } else {
        throw new Error('Unexprected Error')
    }
})

router.get('/', async (_req, res) => {
    const products = await productService.findAll()
    res.status(200).json(products)
})

router.get('/:id', async (req, res) => {
    const product = await productService.findOne(req.params.id)
    res.status(200).json(product)
})

router.put('/:id', async (req, res) => {
    const product = { id: req.params.id, ...req.body }
    const storedProduct = await productService.updateProduct(product)
    if (storedProduct.rowCount === 1) {
        res.status(200).send('Updated')
    } else {
        throw new Error('Unexprected Error')
    }
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    await productService.deleteById(id)
    res.status(200).send('Deleted')
})

export default router