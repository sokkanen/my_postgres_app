import { Router } from 'express'
import { authorize } from '../middlewares.js'

const router = Router()

router.use(authorize)

router.get('/', async (req, res) => {
    res.status(200).send('Hello from protected router!')
})

export default router