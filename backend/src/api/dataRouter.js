import { Router } from 'express'
import { authorize } from '../middlewares.js'
import { insertData, getData } from '../services/dataService.js'

const router = Router()

router.use(authorize)

router.post('/', async (req, res) => {
    const result = await insertData(req.body)
    res.status(201).send('OK')
})

router.get('/', async (_req, res) => {
    const data = await getData()
    res.status(200).send(data)
})

export default router