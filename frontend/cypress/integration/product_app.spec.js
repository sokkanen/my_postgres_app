/* eslint-disable no-undef */

describe('Product management ', () => {
    it('front page can be opened', () => {
      cy.visit('http://localhost:3000')
      cy.contains('Product maintenance application (For internal use only!)')
      cy.contains('All products')
    })

    it('new product can be added', () => {
        cy.get('#name').type('testituote')
        cy.get('#price').type(1000)
        cy.get('#add-button').click()
        cy.contains('testituote')
        cy.contains('10,00')
        cy.get('*[id^="testituote_"]')
    })
})