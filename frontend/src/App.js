import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { 
  Container,
  Navbar,
  Row,
  Col,
  Button
} from 'react-bootstrap'
import * as Icons from 'react-bootstrap-icons'
import { 
  BrowserRouter as Router, 
  Routes, 
  Route,
  Link
} from 'react-router-dom'
import Products from './components/Products';
import Query from './components/Query';
import { ContextProvider, AppContext } from './AppContext'
import { useContext, useEffect } from 'react';
import Home from './components/Home';

const Navigation = () => {
  const [state, setState] = useContext(AppContext)
  const handleLogOut = () => {
    setState((state) => {
      return { ...state, loggedIn: false }
    })
    document.cookie = "app-token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  }
  useEffect(() => {
    if (document.cookie.includes('app-token')) {
      setState((state) => {
        return { ...state, loggedIn: true }
      })
    }
  }, [setState])
  return (
    <Navbar Navbar bg="dark" variant="dark">
      <Container >
        <Container style={{ margin: '5px', padding: '15px' }}>
          <Row>
            <Col><Link to={"/"}><Icons.HouseDoor style={{ marginRight: '10px' }}/>Home</Link></Col>
            <Col><Link to={"/products"}><Icons.Cart style={{ marginRight: '10px' }}/>Products</Link></Col>
            <Col><Link to={"/query"}><Icons.BarChartFill style={{ marginRight: '10px' }}/>Query</Link></Col>
          </Row>
        </Container>
        <Navbar.Brand>Product maintenance application (For internal use only!)</Navbar.Brand>
        {state.loggedIn ? <Button onClick={handleLogOut}>LogOut!</Button> : null}
      </Container>
    </Navbar>
  )
}

const App = () => {
  return (
    <div className="App">
      <ContextProvider>
        <Router>
          <Navigation/>
          <Container fluid>
            <Routes>
              <Route path="/" element={<Home/>}></Route>
              <Route path="/products" element={<Products/>}></Route>
              <Route path="/query" element={<Query/>}></Route>
            </Routes>
          </Container>
        </Router>
      </ContextProvider>
    </div>
  );
}

export default App;
