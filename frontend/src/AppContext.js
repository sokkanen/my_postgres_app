import { createContext, useState } from "react";

const context = {}
const changeContext = () => {}
export const AppContext = createContext([context, changeContext])

export const ContextProvider = props => { 
    const initialState = {
        loggedIn: false
    }
    const [state, setState] = useState(initialState)
    return (
        <AppContext.Provider value={[state, setState]}>
            {props.children}
        </AppContext.Provider>
    )
}