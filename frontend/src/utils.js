export const convertToEuroString = (numbers) => {
    const euros = Math.floor(numbers / 100)
    const cents = numbers % 100
    return `${euros},${cents < 10 ? '0' + cents : cents}`
}