import axios from 'axios'

// Creates an Axios instance, which does not send cookies.
const client = axios.create({
    withCredentials: false
})

export default client