import axios from '../clients/noCredentialsAxios.js'

export const login = async (user) => {
    try {
        const response = await axios.post('/user/login', user)
        return response.status === 200 ? true : false
    } catch (error) {
        return false
    }

}