import axios from '../clients/noCredentialsAxios.js'

export const getAll = async () => {
    const response = await axios.get('/products')
    return response.data
}

export const removeOne = async (id) => {
    const response = await axios.delete(`/products/${id}`)
    return response.status
}

export const addOne = async (product) => {
    const response = await axios.post(`/products/`, product)
    return response.data.id
}