import axios from 'axios'

const getAll = async () => {
    const response = await axios.get('/data')
    return response.data
}

const sendData = async (data) => {
    try {
        const response = await axios.post(`/data`, data)
        return response
    } catch (error) {
        return null
    }
}

const appService = { getAll, sendData }

export default appService