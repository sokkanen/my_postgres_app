import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { useEffect, useState } from 'react'
import dataService from '../services/dataService.js'

ChartJS.register(ArcElement, Tooltip, Legend);

const colors = [
  'rgba(255, 99, 132, 0.2)',
  'rgba(54, 162, 235, 0.2)',
  'rgba(255, 206, 86, 0.2)',
  'rgba(75, 192, 192, 0.2)',
  'rgba(153, 102, 255, 0.2)',
  'rgba(255, 159, 64, 0.2)',
  'rgba(255, 1, 64, 0.2)',
]

const DataSet = () => {
  const [data, setData] = useState({})
  const [initialized, setInitialized] = useState(false)
  useEffect(() => {
    const getAllData = async () => {
      const response = await dataService.getAll()
      const initialData = {
        labels: [
              'Yleinen ohjelmistoarkkitehtuuri', 
              'GitLab CI/CD', 
              'Express middlewaret, kutsujen käsittely, ym.',
              "Azuren ihmeellisyydet (hämmästellään yhdessä)",
              'React useContext ym. hook-asiat', 
              'React router',
              "Ei kertaustarpeita."
          ]
      }
      const datasets = [
        {
          id: 'primary',
          label: 'Ensisijainen',
          backgroundColor: colors,
          borderColor: colors,
          borderWidth: 1,
        },
        {
          id: 'secondary',
          label: 'Toissijainen',
          backgroundColor: colors,
          borderColor: colors,
          borderWidth: 1,
        },
      ]
      datasets[0].data = response.primary
      datasets[1].data = response.secondary
      setData({ ...initialData, datasets: datasets })
      setInitialized(true)
    }
    if (!initialized) {
      getAllData()
    }
  }, [initialized])
  return <>{initialized ? <Pie style={{ marginTop: '4em' }} datasetIdKey='id' data={{ ...data }}/> : null}</>
}

export default DataSet