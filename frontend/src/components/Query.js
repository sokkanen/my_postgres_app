import { useContext, useState } from 'react'
import { AppContext } from '../AppContext'
import { Form, Button } from 'react-bootstrap'
import DataSet from './DataSet'
import dataService from '../services/dataService.js'
import Login from './Login'

const QueryForm = ({ title, id }) => {
    return (
        <div style={{ marginTop: '4em' }}>
            <h4>{title}</h4>
            <Form.Select id={id} style={{ marginTop: '2em', maxWidth: '100%' }} size="lg" aria-label="Default select example">  
                <option value="0">Yleinen ohjelmistoarkkitehtuuri</option>
                <option value="1">GitLab CI/CD</option>
                <option value="2">Express middlewaret, kutsujen käsittely, ym.</option>
                <option value="3">Azuren ihmeellisyydet (hämmästellään yhdessä)</option>
                <option value="4">React useContext ym. hook-asiat</option>
                <option value="5">React router</option>
                <option value="6">Ei kertaustarpeita.</option>
            </Form.Select>
        </div>
    )
}

const Query = () => {
    const [state, setState] = useContext(AppContext)
    const [sent, setSent] = useState(false)

    const handleSendQuery = async (event) => {
        event.preventDefault()
        const data = {
            primary: event.target.primary.value,
            secondary: event.target.secondary.value
        }
        const response = await dataService.sendData(data)
        if (response !== null) {
            setSent(true)
        } else {
            setState((state) => {
                return { ...state, loggedIn: false }
            })
        }
    }

    if (!state.loggedIn) {
        return <Login/>
    }

    return (
        <div style={{ display: 'inline-block'}}>
            <h1 style={{ marginTop: '2em' }}>Kertauskysely</h1>
            <h2 style={{ marginTop: '2em' }}>Kertauskoulutus järjestetään kurssiviikolla 9 vapaamuotoisena koulutuksena.</h2>
            <h2 >Koulutus on täysin vapaaehtoista. Aika tarkentuu viikon 9 alussa.</h2>
            { !sent ? 
                    <>
                    <Form onSubmit={handleSendQuery}>
                        <QueryForm title="Mistä asiasta ensisijaisesti haluaisit kertauskoulutusta?" id="primary"/>
                        <QueryForm title="Entä mistä toissijaisesti?" id="secondary"/>
                        <Button style={{ marginTop: '2em' }} type="submit">Lähetä</Button>
                    </Form>
                    <Button
                        onClick={() => setSent(true)}
                        style={{ marginTop: '2em' }}
                    >Olen jo vastannut, ja haluan vain nähdä vastaukset
                    </Button> 
                    </> :
                    <h2 style={{ marginTop: '4em' }}>Kiitos vastauksistasi!</h2>
            }
            {sent ? <DataSet/> : null}
        </div>
    )
}

export default Query