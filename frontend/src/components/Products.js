import { useState, useEffect } from 'react'
import { getAll, removeOne, addOne } from '../services/productService'
import { 
  Table, 
  Row, 
  Col,
  Button,
} from 'react-bootstrap'
import { convertToEuroString } from '../utils'
import FormComponent from './FormComponent'


const Products = () => {
    const [products, setProducts] = useState([])
    useEffect(() => {
        const getAllProducts = async () => {
            const products = await getAll()
            setProducts(products)
        }
        getAllProducts()
    }, [])

    const handleRemove = async (event, id) => {
        event.preventDefault()
        const status = await removeOne(id)
        status === 200 && setProducts((products) => {
            return products.filter(p => p.id !== id)
        })
    }

    const handleAdd = async (event) => {
        event.preventDefault()
        const product = {
            name: event.target.name.value,
            price: event.target.price.value
        }
        event.target.name.value = event.target.price.value = ''
        const id = await addOne(product)
        setProducts((products) => {
            return [...products, {...product, id}]
        })
    }

    const formProps = {
        handlerFunction: handleAdd,
        labelOne: 'Product name',
        labelTwo: 'Price',
        phOne: 'Product',
        phTwo: 'Price in cents',
        idOne: 'name',
        idTwo: 'price',
        typeOne: 'text',
        typeTwo: 'number',
        buttonId: 'add-button',
        buttonText: 'Add'
    }

    return (
        <>
            <Row>
                <Col>
                    <h1>All products</h1>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th style={{ maxWidth: '3em' }}>#</th>
                            <th>Product</th>
                            <th>Price in €</th>
                            <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map((product) => {
                                return (
                                    <tr key={product.id}>
                                        <td id={`${product.name}_${product.id}`}>{product.id}</td>
                                        <td>{product.name}</td>
                                        <td>{convertToEuroString(product.price)}</td>
                                        <td><Button 
                                            onClick={(e) => handleRemove(e, product.id)} 
                                            variant='warning'
                                            id={`remove-${product.id}`}
                                        >Remove</Button></td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row style={{ marginTop: '4em' }}>
                <Col>
                    <h2>Add a new product</h2>
                </Col>
            </Row>
            <Row>
                <Col></Col>
                <Col>
                    <FormComponent {...formProps}/>
                </Col>
                <Col></Col>
            </Row>
        </>
    )
}

export default Products