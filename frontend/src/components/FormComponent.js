import { 
    Button,
    Form
} from 'react-bootstrap'

const FormComponent = ({
    handlerFunction,
    labelOne,
    labelTwo,
    phOne,
    phTwo,
    idOne,
    idTwo,
    typeOne,
    typeTwo,
    buttonId,
    buttonText
}) => {

    return (
        <Form onSubmit={handlerFunction}>
            <Form.Group className="mb-3">
                <Form.Label>{labelOne}</Form.Label>
                <Form.Control id={idOne} type={typeOne} placeholder={phOne} />
            </Form.Group>
            <Form.Group className="mb-3">
                <Form.Label>{labelTwo}</Form.Label>
                <Form.Control id={idTwo} type={typeTwo} placeholder={phTwo} />
            </Form.Group>
            <Button id={buttonId} variant="primary" type="submit">
                {buttonText}
            </Button>
        </Form>
    )
}

export default FormComponent