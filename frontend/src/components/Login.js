/*eslint no-unused-vars: "off"*/

import FormComponent from './FormComponent'
import { useContext } from 'react'
import { AppContext } from '../AppContext'
import { 
    Container, 
    Row,
    Col
} from 'react-bootstrap'
import { login } from '../services/userService'

const Login = () => {
    const [state, setState] = useContext(AppContext)

    const handleLogin = async (event) => {
        event.preventDefault()
        const user = {
            username: event.target.username.value,
            password: event.target.password.value
        }
        event.target.username.value = ''
        event.target.password.value = ''
        const success = await login(user)
        if (!success) {
            alert('Wrong username or password!')
        } else {
            setState((state) => {
                return {...state, loggedIn: true}
            })
        }
    }

    const formProps = {
        handlerFunction: handleLogin,
        labelOne: 'Username',
        labelTwo: 'Password',
        phOne: 'Username',
        phTwo: 'Password',
        idOne: 'username',
        idTwo: 'password',
        typeOne: 'text',
        typeTwo: 'password',
        buttonId: 'login-button',
        buttonText: 'Login'
    }

    return (
        <Container style={{ marginTop: '2em' }}>
            <Row>
                <Col></Col> {/* Not the prettiest... */}
                <Col><FormComponent {...formProps}/></Col>
                <Col></Col>
            </Row>
        </Container>

    )
}

export default Login